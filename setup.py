
from peewee import Column, CharField, Model, SqliteDatabase, AutoField

file_maindb = open('main.db', 'w+')
file_dataset = open('dataset_faces.dat', 'w+')

db = SqliteDatabase('main.db')
db.connect()


class User(Model):
    class Meta:
        database = db

    id = AutoField()
    name = CharField()
    picture = CharField()
    id_condominio = CharField()
    id_morador = CharField()


db.create_tables([User])