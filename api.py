import face_recognition
from flask import Flask, jsonify, request, redirect
from peewee import Column, CharField, Model, SqliteDatabase, AutoField
import pickle
import numpy as np
import math
import os

file_maindb = open('main.db', 'w+')
file_dataset = open('dataset_faces.dat', 'w+')

db = SqliteDatabase('main.db')
db.connect()

class User(Model):
    class Meta:
        database = db

    id = AutoField()
    name = CharField()
    picture = CharField()
    id_condominio = CharField()
    id_morador = CharField()

db.create_tables([User])


# You can change this to any folder on your system
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}

app = Flask(__name__)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/recognizer', methods=['POST'])
def upload_image():
    # Check if a valid image file was uploaded
    if request.method == 'POST':
        if 'file' not in request.files:
            return redirect(request.url)

        file = request.files['file']

        if file.filename == '':
            return redirect(request.url)

        if file and allowed_file(file.filename):
            # The image file seems valid! Detect faces and return the result.
            return detect_faces_in_image(file)


def detect_faces_in_image(file_stream):
    # Carrega todos os encondings de todos os usuários cadastrados
    with open('dataset_faces.dat', 'rb') as f:
        all_face_encodings = pickle.load(f)
    f.close()
    # Carrega econdings na lib
    face_encodings = np.array(list(all_face_encodings.values()))

    # Lista os nomes dos arquivos
    face_names = list(all_face_encodings.keys())

    # Carrega imagem enviada para a API
    img = face_recognition.load_image_file(file_stream)

    # Gera encondings para a imagem que foi feito upload
    unknown_face_encodings = face_recognition.face_encodings(img)

    face_found = False
    face_id = ""

    if len(unknown_face_encodings) > 0:
        match_results = face_recognition.compare_faces(
            face_encodings, unknown_face_encodings[0])

        names_with_result = list(zip(face_names, match_results))

        for result in names_with_result:
            if result[1] == True:
                face_found = True
                face_id = result[0]
                break

    results = {
        "face_found": face_found,
        "face_id": face_id,
    }

    return jsonify(results)


@app.route('/register', methods=['POST'])
def upload_image_register():

    if 'file' not in request.files:
        return redirect(request.url)

    file = request.files['file']

    if file.filename == '':
        return redirect(request.url)

    fullFileName = './pictures/%s' % (file.filename)

    raw = file.read()

    filename, file_extension = os.path.splitext(file.filename)

    final_filename = "./pictures/%s_%s%s" % (request.values['id_condominio'], request.values['id_morador'], file_extension)
    path_filename = "pictures/%s_%s%s" % (request.values['id_condominio'], request.values['id_morador'], file_extension)

    if file and allowed_file(file.filename):
        with open(final_filename, 'wb') as f:
            f.write(raw)

        return register_face(file, request.values['name'], request.values['id_morador'], request.values['id_condominio'], path_filename)


def register_face(file_stream, name, id_morador, id_condominio, picture):

    datafile = 'dataset_faces.dat'

    if os.path.getsize(datafile) > 0:
        with open(datafile, 'rb') as f:
            all_face_encodings = pickle.load(f)
    else:
        all_face_encodings = {}

    img = face_recognition.load_image_file(file_stream)
    all_face_encodings[name] = face_recognition.face_encodings(img)[0]

    new_user = User(name=name, id_condominio=id_condominio,
                    picture=picture, id_morador=id_morador)
    new_user.save()

    response = {
        "id": new_user.id,
        "picture": new_user.picture,
        "name": new_user.name,
        "id_condominio": new_user.id_condominio,
        "id_morador": new_user.id_morador
    }

    with open(datafile, 'wb') as f:
        pickle.dump(all_face_encodings, f)
    f.close()

    return jsonify(response)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5001, debug=True)
